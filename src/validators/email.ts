import { FormControl } from '@angular/forms';
import { ApiProvider } from '../providers/api/api';
import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS} from '@angular/forms';



export class EmailValidator {


   static isValid(c: FormControl): any {
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  
    return EMAIL_REGEXP.test(c.value) ? null : {
      validateEmail: {
        valid: false
      }
    };
    
    }
}