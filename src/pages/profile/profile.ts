import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api'
import {EdituserprofilePage} from '../edituserprofile/edituserprofile'
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider) {
    this.profile = navParams.data[0];
    this.profile.avatar = this.profile.avatar;
    console.log(this.profile.name);
  }
  setup(){
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  editprofile() {
    this.navCtrl.push(EdituserprofilePage);
  }


}
