import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EdituserprofilePage } from './edituserprofile';

@NgModule({
  declarations: [
    EdituserprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(EdituserprofilePage),
  ],
})
export class EdituserprofilePageModule {}
