import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api'
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-all-tasks',
  templateUrl: 'all-tasks.html',
})
export class AllTasksPage {

  tasks:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider) {
    this.getAllTasks();
  }

  getAllTasks(){
    this.api.getTasks().subscribe((tasks)=>{
      this.tasks = tasks;
      console.log(this.tasks);
    });
    //this.tasks = this.api.getTasks();
  }

  onProfileClick(user:any){
    this.navCtrl.push(ProfilePage,user);
  }

  onTaskClick(task:any){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllTasksPage');
  }

}
