import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../../providers/api/api';
import { CategoryPage } from "../category/category";
import { ProfilePage } from "../profile/profile";
import {AllTasksPage} from "../all-tasks/all-tasks"
import { StartPage } from '../start/start';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  categories: any;
  user:any;
  constructor(public navCtrl: NavController, private api: ApiProvider) {
    api.getCategories().toPromise().then(res=>{
      this.categories = res;
      this.categories.forEach(element => {
        element.image = 'http://test.taskhunter.pl/'+element.image;
      });
      console.log( this.categories);
    });
    //this.categories = api.getCategories();
    
  }

  onCatClick(category: any) {
    //console.log(category);
    this.navCtrl.push(CategoryPage, category);
  }
  openWelcome(){
    this.navCtrl.setRoot(StartPage,this.user);
  }
  openProfile() {
    this.navCtrl.push(ProfilePage,this.user);
  }
  openAllTasks() {
    this.navCtrl.push(AllTasksPage);
  }
}
