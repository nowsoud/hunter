import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartPage } from './start';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
@NgModule({
  declarations: [
    StartPage,
  ],
  imports: [
    IonicPageModule.forChild(StartPage),
  ],
  providers:[
    UniqueDeviceID
  ]
})
export class StartPageModule {}
