import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login'
import {RegisterPage} from '../register/register'
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import {Platform} from 'ionic-angular'; //for detect OS
import { Storage } from '@ionic/storage';
import { LoginData } from '../../types/LoginData';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})

export class StartPage {
  device: any;
  uuid:any;
  hid:any;
  data:LoginData = new LoginData();
  constructor(private storage: Storage, private uniqueDeviceID: UniqueDeviceID, public navCtrl: NavController, public navParams: NavParams) {
    
    storage.get("token").then(res=>
      {
        if(!res){
          console.log('token is not exist ');
        }else{
          console.log('token is', res);
          this.hid = res;
        }
      });
      storage.get("login_data").then(res=>
        {
          if(!res){
            console.log('login data is not exist ');
          }else{
            console.log('lodin data is', res);
            this.data = res;
          }
        });
        

    this.uniqueDeviceID.get()
    .then((uuid: any) => {
      console.log(uuid);
      this.uuid = uuid;
    })
    .catch((error: any) => console.log(error));
  }
  fakeHid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }

  click(){
    this.navCtrl.push(LoginPage);
  }

  reg(){
    this.navCtrl.push(RegisterPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad StartPage');
  }

}
