import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-create-task',
  templateUrl: 'create-task.html',
})
export class CreateTaskPage {
  cat: any;
  data = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    this.cat = navParams.data;
  }
  logForm() {
    // this.data =  this.getData();
    console.log(this.data);
    let toast = this.toastCtrl.create({
      message: this.data + ' was added successfully',
      duration: 3000
    });
    toast.present();
  }

}
