import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { LoginResponse } from '../../types/LoginResponse';
import { LoginData } from '../../types/LoginData';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data:LoginData = new LoginData();
  response:LoginResponse ;
  constructor(private alertCtrl: AlertController,private storage: Storage, public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider) {
    storage.get("hid").then(res=>
    {
      this.data.hid = res;
      console.log(res);
    });
  }

  Login(){
    this.api.sendLoginRequest(this.data).subscribe((res)=>{
      this.response = <LoginResponse>res;

      if(this.response.access === true){
        this.navCtrl.setRoot(HomePage);
        this.storage.set("token",this.response.token);
        this.storage.set("login_data",this.data);
      }else{
        this.presentAlert();
        console.log('access denied')
      }
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Access denied',
      subTitle: 'Login or password is incorrect',
      buttons: ['Try again']
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
}