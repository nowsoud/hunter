import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {EmailValidator} from '../../validators/email';
import {PasswordValidator} from '../../validators/password';
import {PhoneValidator} from '../../validators/phone';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { RegistrationResponse } from '../../types/RegistrationRespond';
import { HomePage } from '../home/home';
import { LoginData } from '../../types/RegistrationData';
import { LoginAvailability } from '../../types/LoginAvailability';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  response: RegistrationResponse;
  type:any;
  regForm:FormGroup;
  hid:any;
  submitAttempt: boolean = false;
  loginData:LoginData;
  loginAvalability:LoginAvailability;
  constructor(private alertCtrl: AlertController, private storage: Storage, public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams, public api:ApiProvider) {
    this.type = "Imię nazwisko";
    this.regForm = formBuilder.group({
      name:['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      login: ['', EmailValidator.isValid],
      password:['', Validators.minLength(8)],
      confirm:['',Validators.minLength(8)],
      phone:['',PhoneValidator.isValid]
  });
  storage.get('hid').then(res=>{
    this.hid = res;
  });
  }
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['Try again']
    });
    alert.present();
  }

  registration(){
    let type = (this.type==="Imię nazwisko")?0:1;
    let data = this.regForm.value;
    data.type = type;
    data.hid = this.hid;
    this.loginData = <LoginData>data;
    console.log(this.loginData);
    
    this.validation(data);
    
  }
  registrate(data){
    this.api.sendRegistrationRequest(data).subscribe((res)=>{
      this.response = <RegistrationResponse>res;

      if(this.response.access === true){
        this.navCtrl.setRoot(HomePage);
        this.storage.set("token",this.response.token);
        this.storage.set("login_data",this.loginData);
      }else{
        console.log('access denied')
      }
      
      console.log(res);
    })
  }
  validation(data){
    this.api.checkEmail(data.login).subscribe(res=>{
      this.loginAvalability =  <LoginAvailability>res;
      console.log(this.loginAvalability);

      if(data.name==""){
        this.presentAlert("Name is empty");
        return;
      }
      if(data.login==""){
        this.presentAlert("Login is empty");
        return;
      }
      if(data.password==""){
        this.presentAlert("Password is empty");
        return;
      }
      if(data.password!=data.confirm){
        this.presentAlert("Password does not match the confirm password");
        return;
      }

      if(this.loginAvalability.availability === false){
        this.presentAlert("Email is already used!");
        return false;
      }else{
        this.registrate(data);
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
