import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { collectExternalReferences } from '@angular/compiler/src/output/output_ast';
import {CreateTaskPage} from '../create-task/create-task'
import { ApiProvider } from '../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  category:any;
  cats:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, api:ApiProvider) {
    this.category = navParams.data;
    api.getSubCategories(navParams.data).subscribe(res=>{
      this.cats = res;
      console.log(this.cats)
    });
    //console.log(this.category)
  }

  click(category:any){
    this.navCtrl.push(CreateTaskPage,category);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }

}
