import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {CategoryPage}from "../pages/category/category";
import {CreateTaskPage} from '../pages/create-task/create-task'
import {ProfilePage} from '../pages/profile/profile'
import { ApiProvider } from '../providers/api/api';
import { EdituserprofilePage } from '../pages/edituserprofile/edituserprofile';
import { HttpClientModule } from '@angular/common/http';
import { Usercomp } from '../app/user/user.comp';
import {StartPage} from '../pages/start/start';
import {LoginPage} from '../pages/login/login';
import {RegisterPage} from '../pages/register/register';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { IonicStorageModule } from '@ionic/storage';
import { AllTasksPage } from '../pages/all-tasks/all-tasks';
import { AllTasksPageModule } from '../pages/all-tasks/all-tasks.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CategoryPage,
    CreateTaskPage,
    ProfilePage,
    EdituserprofilePage,
    Usercomp,
    StartPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AllTasksPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CategoryPage,
    CreateTaskPage,
    ProfilePage,
    EdituserprofilePage,
    Usercomp,
    StartPage,
    LoginPage,
    RegisterPage,
    AllTasksPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    UniqueDeviceID
  ]
})
export class AppModule {}