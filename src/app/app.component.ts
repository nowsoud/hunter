
import {StartPage} from '../pages/start/start'
import { HomePage } from '../pages/home/home';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProfilePage } from '../pages/profile/profile';
import { AllTasksPage } from '../pages/all-tasks/all-tasks';
import { ApiProvider } from '../providers/api/api';
import { Storage } from '@ionic/storage/dist/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;
  token:any;

  constructor(private storage:Storage, private api:ApiProvider, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Profile', component: ProfilePage },
      { title: 'Tasks', component: AllTasksPage },
      { title: 'LogOut', component: StartPage }
    ];

    storage.get("token").then(res=>
      {
        if(!res){
          console.log('token is not exist ');
          this.rootPage = StartPage;
        }else{
          console.log('token is', res);
          this.rootPage = HomePage;
          this.token = res;
        }
      });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title == 'Profile'){
      this.api.getUserProfile(this.token).subscribe(res=>{
        this.nav.push(page.component, res);
        return;
      });
    }else if(page.title== 'LogOut'){
      this.storage.clear();
      this.nav.setRoot(page.component);
    } else {
      this.nav.push(page.component);
    }
  }

initializeApp() {
  this.platform.ready().then(() => {
    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.
    this.statusBar.styleDefault();
    this.splashScreen.hide();
  });
}
}
