import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginData } from '../../types/LoginData';
import { Storage } from '@ionic/storage/dist/storage';

@Injectable()
export class ApiProvider {
  key:any = "f5b201dd425a9469fda211a5342cb40f";
  cat_url:any = 'http://test.taskhunter.pl/api/task/categories?key='+this.key;
  tasks_url:any = 'http://test.taskhunter.pl/api/task/tasks?key='+this.key+'&cid=38';
  user_profile_url:any = 'http://test.taskhunter.pl/api/profile/my-profile?key='+this.key+'&token=';

  cat_mock:any = [
    {
        id:1,
        img:"https://home-theater-design-concepts.com/wp-content/uploads/2017/05/wine-barrel-theme-home-theater.jpg",
        title:"Usługi dla domu",
        sub_categories:[
            {
                id:2,
                title:"Czyszczenie",
                sub_categories:[]
            },
            {
                id:4,
                title:"Niania",
                sub_categories:[]
            }
        ]
    },
    {
        id:3,
        img:"http://blogs.conted.ox.ac.uk/sud/wp-content/uploads/2016/07/Urban-transport-networks.jpg",
        title:"Transport ładunków",
        sub_categories:[
            {
                id:5,
                title:"Dostawa kurierska",
                sub_categories:[]
            }
        ]
    }];
  tasks_mock:any=[
    {
        "id": 1,
        "name": "Догляд за 2 хвилястипи папугами",
        "description": "Їдемо у відпустку. Потрібно від сьогодні до 29.12.17 доглянути за пташками. Нічого особливого: 1 раз на день освіжати корм, воду та висип...",
        "requirements": "белые трусики и тусики тусики",
        "budget": 300,
        "city": {
            "id": 1,
            "title": "Kraków"
        },
        "create_time": "29 декабря 2017",
        "close_time": "29 декабря 2017",
        "state": 2,
        "executor": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Костянтин Чaрка",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Танцую чечетку на дому"
        },
        "user": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Костянтин Чaрка",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://images.kabanchik.ua/ee64d965-5143-4c13-9887-dbc0f6e2d954_150x150.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Танцую чечетку на дому"
        },
        "category": {
            "id": 5,
            "name": "Dostawa kurierska"
        },
        "owner_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://images.kabanchik.ua/ee64d965-5143-4c13-9887-dbc0f6e2d954_150x150.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "executor_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "offers": [
            {
                "id": 10,
                "task_id": 6,
                "user_id": 1,
                "description": "Тратата",
                "price": 11,
                "chat_id": "NULL"
            },
            {
                "id": 2,
                "task_id": 1,
                "user_id": 20,
                "description": "sss",
                "price": 3500,
                "chat_id": "NULL"
            },
            {
                "id": 7,
                "task_id": 1,
                "user_id": 19,
                "description": "Войду на пол шишечки",
                "price": 300,
                "chat_id": "NULL"
            }
        ]
    },
    {
        "id": 1,
        "name": "Выгрузить груз из авто и доставить клиенту",
        "description": "Конфиденциальность Вашей информации и гарнторованное исполнение. В оговоренные сроки времени и согласованную сумму доставлю Ваш заказ к...",
        "requirements": "белые трусики и тусики тусики",
        "budget": 500,
        "city": {
            "id": 1,
            "title": "Kraków"
        },
        "create_time": "21 ноября 2017",
        "close_time":   "21 ноября 2017",
        "state": 2,
        "executor": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Николай Слякоть",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://images.kabanchik.ua/ee64d965-5143-4c13-9887-dbc0f6e2d954_150x150.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Перспектива и мололол"
        },
        "user": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Николай Слякоть",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://images.kabanchik.ua/650f623e-cf2b-439a-8b35-2d6fd395e299.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Перспектива и мололол"
        },
        "category": {
            "id": 5,
            "name": "Dostawa kurierska"
        },
        "owner_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "executor_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "offers": [
            {
                "id": 10,
                "task_id": 6,
                "user_id": 1,
                "description": "Тратата",
                "price": 11,
                "chat_id": "NULL"
            }
        ]
    },
    {
        "id": 1,
        "name": "Предоставлю склад/организую отправку",
        "description": "Организую хранение, отправку Новой почтой, выдачу клиентам негабаритного товара. Приблизительная площадь для хранения: 2 метра ...",
        "requirements": "белые трусики и тусики тусики",
        "budget": 300,
        "city": {
            "id": 1,
            "title": "Kraków"
        },
        "create_time": "21 ноября 2017",
        "close_time": "21 ноября 2017",
        "state": 2,
        "executor": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Николай Слякоть",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Перспектива и мололол"
        },
        "user": {
            "user_id": 1,
            "phone": "‎+48566932115",
            "name": "Анатолий Симонов",
            "city": {
                "id": 1,
                "name": "Krakow"
            },
            "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
            "site": "www.nikitalox.com",
            "adress": "Mlynska 6/78",
            "postal_code": "361-56",
            "idently_code": "‎22569865455",
            "name_employeer": "Афанасий Бомж",
            "about": "Перспектива и мололол"
        },
        "category": {
            "id": 5,
            "name": "Dostawa kurierska"
        },
        "owner_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "executor_review": {
            "id": 1,
            "from_user": {
                "user_id": 1,
                "phone": "‎+48566932115",
                "name": "Николай Слякоть",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "to_user": {
                "user_id": 2,
                "phone": "‎+48566932115",
                "name": "Лисий Мороз",
                "city": {
                    "id": 1,
                    "name": "Krakow"
                },
                "avatar": "https://pp.userapi.com/c841222/v841222798/31e1/TbV8zIoWTjM.jpg",
                "site": "www.nikitalox.com",
                "adress": "Mlynska 6/78",
                "postal_code": "361-56",
                "idently_code": "‎22569865455",
                "name_employeer": "Афанасий Бомж",
                "about": "Перспектива и мололол"
            },
            "task_id": 16,
            "text": "qwertyu",
            "creation_date": "12:34 03/07/2017"
        },
        "offers": [
            {
                "id": 10,
                "task_id": 6,
                "user_id": 1,
                "description": "Тратата",
                "price": 11,
                "chat_id": "NULL"
            },
            {
                "id": 2,
                "task_id": 1,
                "user_id": 20,
                "description": "sss",
                "price": 3500,
                "chat_id": "NULL"
            },
            {
                "id": 7,
                "task_id": 1,
                "user_id": 19,
                "description": "Войду на пол шишечки",
                "price": 300,
                "chat_id": "NULL"
            },
            {
                "id": 8,
                "task_id": 4,
                "user_id": 21,
                "description": "Zrobię to",
                "price": 200,
                "chat_id": "NULL"
            },
            {
                "id": 9,
                "task_id": 7,
                "user_id": 19,
                "description": "Zrobię",
                "price": 150,
                "chat_id": "NULL"
            }
        ]
    }];
    user_mock:any={
    "user_id": 1,
    "phone": "‎+48566932115",
    "name": "Валерия Колонкова",
    "city": {
        "id": 1,
        "name": "Krakow"
    },
    "avatar": "http://mama-klub.ru/images/kosmetizka/kvadrat.jpg",
    "site": "www.nikitalox.com",
    "adress": "Mlynska 6/78",
    "postal_code": "361-56",
    "idently_code": "‎22569865455",
    "name_employeer": "Афанасий Бомж",
    "about": "Крупный производитель видеокарт Nvidia обратился к некоторым европейским ритейлерам с просьбой принять меры для ограничения продаж видеокарт, пользующихся высоким спросом среди майнеров."
}


  constructor(storage:Storage, public http: HttpClient) {

  }
  getCategories(){
    //return this.cat_mock;
    return this.http.get(this.cat_url);
  }
  getSubCategories(category){
    //return this.cat_mock;
    let url = 'http://test.taskhunter.pl/api/task/category?key=f5b201dd425a9469fda211a5342cb40f&id='+category.id;
    console.log("UrL : "+url);
    return this.http.get(url);
  }
  getTasks(){
    return this.http.get(this.tasks_url);
    //return this.tasks_mock;
  }
  getUserProfile(token){
    return this.http.get(this.user_profile_url+token);
  }
  sendLoginRequest(data:LoginData){
    console.log(data);
    let request = "http://test.taskhunter.pl/api/auth/login?key="+this.key+"&login="+data.login+"&pass="+data.password;
    return this.http.get(request);
  }
  sendRegistrationRequest(data){
    let request = "http://test.taskhunter.pl/api/auth/registration?key="+this.key+"&login="+data.login+"&pass="+data.password+"&name="+data.name+"&type="+data.type+"&phone="+data.phone;
    return this.http.get(request);
  }
  checkEmail(email){
    let request = "http://test.taskhunter.pl/api/auth/availability?key="+this.key+"&login="+email;
    return this.http.get(request);
  }}
